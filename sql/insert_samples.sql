INSERT INTO ADMIN VALUES ('admin', 'admin');
INSERT INTO USERS VALUES
('user1', '1234', 'minjee9108@hotmail.com', 'jo', 'joseph', 
'smith', '1990-11-8', '24', 'kent st', 'sydney', 2000, 'NSW',
123456789123, 'FALSE', 'CUSTOMER', 'TRUE');
INSERT INTO USERS VALUES
('seller1', 'seller1', 'minjee9108@hotmail.com', 'jo', 'joseph', 
'smith', '1990-11-8', '24', 'kent st', 'sydney', 2000, 'NSW',
123456789123, 'FALSE', 'SELLER', 'TRUE');

INSERT INTO ENTRIES (ID, SELLER, TITLE)
VALUES (0, 'seller1', 'Sample');
INSERT INTO BOOKS VALUES (0);

INSERT INTO ENTRIES (SELLER, TITLE, AUTHORS)
VALUES ('seller1', 'wind up bird chronicle', 'haruki murakami');

insert into books 
select id from entries 
where types = 'BOOK' or types = 'INCOLLECTION';

insert into journals
select id from entries
where types = 'article';

insert into conferences
select id from entries
where types = 'inproceedings' or types = 'proceedings';

select count(*) from entries;
select count(*) from books;
select count(*) from journals;

truncate table entries;
truncate table books;
truncate table journals;

select * from cart_history;