package edu.unsw.comp9321.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;


/**
 * @author srikumarv
 * This class looks up the database via JNDI and returns a connection to the DAO Implementation class
 * 
 */
public class DBConnectionFactory {
	static Logger logger = Logger.getLogger(DBConnectionFactory.class.getName());
	private static Connection con;
	private static DBConnectionFactory factory = null;
	
	private DBConnectionFactory() throws Exception{
	    try {
	        Class.forName("com.mysql.jdbc.Driver").newInstance();
	        System.out.println("Driver loading success!");
	        String url = "jdbc:mysql://localhost:3306/book_site";
	        String name = "root";
	        String password = "mayday";
	        con = DriverManager.getConnection(url, name, password);
	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	        logger.severe("Driver class not found"+e.getMessage());
	    } catch (SQLException e) {
        	logger.severe("Can't connect to databse, throwing exception"+e.getMessage());
            e.printStackTrace();
        }
	}
	
	public static Connection getConnection() throws Exception{
		if (factory == null)
			factory = new DBConnectionFactory();
		return con;
	}
}
